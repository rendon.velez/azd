// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config: {
    tenant: '248e0e49-91e5-4542-97cb-566b9a6166e5',
    clientId: '453f4b4a-5df1-41ac-b487-f2ada9304660',
    endpoints: {
      'http://localhost:4200/': '248e0e49-91e5-4542-97cb-566b9a6166e5',
      'http://localhost:4200/welcome': '248e0e49-91e5-4542-97cb-566b9a6166e5',
      'http://localhost:4200/home': '248e0e49-91e5-4542-97cb-566b9a6166e5',
      'http://localhost:4200/path2': '248e0e49-91e5-4542-97cb-566b9a6166e5',
      'http://localhost:4200/path3': '248e0e49-91e5-4542-97cb-566b9a6166e5',
      'http://localhost:4200/path4': '248e0e49-91e5-4542-97cb-566b9a6166e5',
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
