import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { Prueba2Component } from './prueba2/prueba2.component';
import { Prueba3Component } from './prueba3/prueba3.component';
import { Prueba4Component } from './prueba4/prueba4.component';
import { Prueba1Component } from './prueba1/prueba1.component';
import { HttpClient } from '@angular/common/http';
import { Adal8HTTPService } from 'adal-angular8/adal8-http.service';
import { Adal8Service } from 'adal-angular8/adal8.service';
import { HttpClientModule } from '@angular/common/http';
import { Adal8Guard } from 'adal-angular8';
import { NavbarComponent } from './navbar/navbar.component';
import { GoodByeComponent } from './good-bye/good-bye.component';
import { WelcomeComponent } from './welcome/welcome.component';


@NgModule({
  declarations: [
    AppComponent,
    Prueba2Component,
    Prueba3Component,
    Prueba4Component,
    Prueba1Component,
    NavbarComponent,
    GoodByeComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    Adal8Service,
    Adal8Guard,
    { provide: Adal8HTTPService, useFactory: Adal8HTTPService.factory, deps: [HttpClient, Adal8Service] }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
