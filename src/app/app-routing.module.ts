import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdalGuardGuard } from './adal-guard.guard';
import { GoodByeComponent } from './good-bye/good-bye.component';
import { Prueba1Component } from './prueba1/prueba1.component';
import { Prueba2Component } from './prueba2/prueba2.component';
import { Prueba3Component } from './prueba3/prueba3.component';
import { Prueba4Component } from './prueba4/prueba4.component';
import { WelcomeComponent } from './welcome/welcome.component';


const routes: Routes = [
  { path: '', redirectTo: 'Welcome', pathMatch: 'full' },
  { path: 'Welcome', component: WelcomeComponent },
  { path: 'home', component: Prueba1Component, canActivate: [AdalGuardGuard] },
  { path: 'path2', component: Prueba2Component, canActivate: [AdalGuardGuard] },
  { path: 'path3', component: Prueba3Component, canActivate: [AdalGuardGuard] },
  { path: 'path4', component: Prueba4Component, canActivate: [AdalGuardGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
