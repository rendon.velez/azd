import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Adal8Service } from 'adal-angular8';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {


  loginApp: boolean = false;
  profileInfo: any;
  private returnUrl: string = null;

  constructor(
    private adalService: Adal8Service,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.adalService.init(environment.config);
    this.adalService.handleWindowCallback(false);
  }

  ngOnInit(): void {
    this.loginApp = this.adalService.userInfo.authenticated
    if (this.loginApp) {
      this.profileInfo = this.adalService.userInfo;
      setTimeout(() => {
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'home';
        this.router.navigate([this.returnUrl]);
      }, 1800);
    }
  }

  logIn() {
    this.adalService.login();
  }
}
