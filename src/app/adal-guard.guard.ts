import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Adal8Service } from 'adal-angular8';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdalGuardGuard implements CanActivate, CanActivateChild {

  constructor(private adalSvc: Adal8Service, private router: Router) {
    this.adalSvc.init(environment.config);
    this.adalSvc.handleWindowCallback(false);
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.adalSvc.userInfo.authenticated) {
      return true;
    } else {
      // this.router.navigate(['welcome'], { queryParams: { returnUrl: state.url } });
      this.router.navigate(['Welcome']);
      return false;
    }
  }

  public canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(childRoute, state);
  }

}