import { Component, OnInit } from '@angular/core';
import { Adal8Service } from 'adal-angular8';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  loginApp: boolean = false;
  profileInfo: any;

  constructor(
    private adalService: Adal8Service
  ) {
    this.adalService.init(environment.config);
    this.adalService.handleWindowCallback();
  }

  ngOnInit(): void {
    this.loginApp = this.adalService.userInfo.authenticated
    if (this.loginApp) {
      this.profileInfo = this.adalService.userInfo;
      console.log(this.profileInfo)
    }
  }

  logOut() {
    alert("You're LogOut");
    this.adalService.logOut();
  }

  logIn() {
    alert("You're be redirect to login");
    this.adalService.login();
  }


}
